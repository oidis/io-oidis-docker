Release Name: com-wui-framework-docker v2019.2.0

com-wui-framework-docker
Description: Docker environment focused on WUI Framework production and R&D needs
Author: Oidis
License: BSD-3-Clause. See LICENSE.txt
Format: source code
Location: source/docker
